# fast-context.kak

## _Dedent-based Kakoune code context info_

This [Kakoune](https://github.com/mawww/kakoune/) plugin infers nested code context by following dedent levels. That is, from the current line backwards, it shows lines where the indentation level decreases.

This idea was originally [posted](https://discuss.kakoune.com/t/context-vim-i-want-this/1307/8?) in the Kakoune community hub by [`@JJK96`](#original-code), who provided the [initial implementation](https://github.com/JJK96/kakoune-config/tree/master/autoload/context) on Github.

## Install

If using [`plug.kak`](https://github.com/andreyorst/plug.kak):
```
plug 'https://gitlab.com/kstr0k/fast-context.kak.git' \
demand fast-context %{
# optional config customization
}
```

Manual installation: place somewhere in `autoload`, and `require-module fast-context` in `kakrc`.

## Usage

### Commands

- `fast-context-show` pops up an info box with the inferred context. The context is inferred based on dedented lines (as seen backwards from the current line). Respects the buffer's `tabstop` settings.
- `fast-context-lock` locks the info box in place (on a per-buffer idle hook)
- `fast-context-unlock` removes the idle hook

The commands above apply per-buffer; to enable `fast-context` globally on every new buffer (*not recommended*),
- `fast-context-global-enable` adds a `BufCreate` hook
- `fast-context-global-disable` undoes `*-enable`

Both apply to new buffers only.

### Options

- `fast_context_max_width = 40`: max context line width
- `fast_context_max_context = 8`: max context header lines
- `fast_context_max_backlines = 200`: max source lines to follow backwards

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license

## Original code

Original code (now rewritten) by [Jan-Jaap Korpershoek](https://github.com/JJK96) / UNLICENSE
