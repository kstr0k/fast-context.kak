declare-option -hidden str fast_context_dir %sh{echo "${kak_source%/*}"}

provide-module fast-context %{

declare-option -docstring 'Max context line width' \
  int fast_context_max_width 40
declare-option -docstring 'Max context header lines' \
  int fast_context_max_context 8
declare-option -docstring 'Max source lines analyzed' \
  int fast_context_max_backlines 200

declare-option -hidden str fast_context_sh_code ''
set global fast_context_sh_code %sh{
  cat "$kak_opt_fast_context_dir"/context
}

def fast-context-show %{
  evaluate-commands -save-regs b %{
    execute-keys -draft <esc> %opt{fast_context_max_backlines} 'K<a-x>"by'
    info -title Context -- %sh{
      eval "$kak_opt_fast_context_sh_code"; set -u
      width=$(( kak_window_width - 15 ))
      test "$width" -le $kak_opt_fast_context_max_width || width=$kak_opt_fast_context_max_width
      printf '%s' "$kak_reg_b" |
        get_chunk_headers $kak_opt_tabstop $width $kak_opt_fast_context_max_context
    }
  }
} -override -docstring %{
Show code context
}

def fast-context-lock %{
  hook -group context buffer NormalIdle .* fast-context-show
} -override -docstring %{
Enable idle hook to show context
}

def fast-context-unlock %{
  rmhooks buffer context
} -override -docstring %{
Remove buffer context idle hook
}

def fast-context-global-enable %{
  hook -group context global BufCreate [^*].* %{
    fast-context-lock
  }
} -docstring %{
Lock context info box by default for every new buffer
}
def fast-context-global-disable %{
  rmhooks global context
} -docstring %{
Undo `fast-context-global-enable`
}

}  # module
